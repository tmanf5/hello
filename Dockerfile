FROM golang:1.17.6-alpine3.15
WORKDIR /build
COPY . .
ENV CGO_ENABLED=0
RUN go build cmd/hello/main.go

FROM scratch
COPY --from=0 /build/main /
EXPOSE 8080
EXPOSE 8090
ENTRYPOINT ["/main"]
