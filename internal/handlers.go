package internal

import (
	"context"
	"fmt"
	"log"

	hellopb "tmanf5/hello/proto/gen"
)

func (s *server) AddEmployee(ctx context.Context, in *hellopb.AddEmployeeRequest) (*hellopb.AddEmployeeReply, error) {
	log.Printf("Employee %s received.", in.GetName())

	return &hellopb.AddEmployeeReply{Message: fmt.Sprintf("Employee %s added. - v2", in.GetName())}, nil
}
