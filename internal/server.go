package internal

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	hellopb "tmanf5/hello/proto/gen"
)

var (
	rpcPort           = 8080
	gwPort            = 8090
	systemMetricsPort = 8100
)

type server struct {
	hellopb.UnimplementedEmployeeServiceServer
}

func CreateDaemon() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", rpcPort))
	if err != nil {
		log.Fatalln("Failed to listen:", err)
	}

	s := grpc.NewServer()
	hellopb.RegisterEmployeeServiceServer(s, &server{})
	log.Printf("Serving gRPC on 0.0.0.0:%d", rpcPort)
	go func() {
		log.Fatalln(s.Serve(lis))
	}()

	conn, err := grpc.DialContext(
		context.Background(),
		fmt.Sprintf("0.0.0.0:%d", rpcPort),
		grpc.WithBlock(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		log.Fatalln("Failed to dial server:", err)
	}

	gwmux := runtime.NewServeMux()
	err = hellopb.RegisterEmployeeServiceHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register gateway:", err)
	}

	gwServer := &http.Server{
		Addr:    fmt.Sprintf(":%d", gwPort),
		Handler: gwmux,
	}

	log.Printf("Serving gRPC-Gateway on http://0.0.0.0:%d", gwPort)
	go func() {
		log.Fatalln(gwServer.ListenAndServe())
	}()

	http.HandleFunc("/v1/live", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	})
	http.HandleFunc("/v1/ready", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	})
	log.Printf("Serving system metrics on http://0.0.0.0:%d", systemMetricsPort)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", systemMetricsPort), nil))
}
